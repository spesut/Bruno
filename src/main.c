
#include "BoardSupportPackage.h"
// FreeRTOS headers
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#include "MS4515Task.h"
#include "MP3V5004Task.h"
#include "PrintTask.h"


// Pin object for PC8 (connected to discovery LED)
GPIO_Pin_Object_t PC8_LED;

// Task to blink and LED
void blinky_task(void* p)
{
  TickType_t xLastWakeTime;
  const TickType_t xFrequency = 100;

  // Get the start time for the task
  xLastWakeTime = xTaskGetTickCount();

  while ( 1 )
  {
    // Toggle the LED and wait until the desired frequency is hit
    GPIO_TogglePin( &PC8_LED );
    vTaskDelayUntil( &xLastWakeTime, xFrequency );
  }
}


int main(void)
{
  // Configure the GPIO pin for pulsing the LED
  GPIO_ConfigurePin( &PC8_LED,
                     GPIO_PORTC,
                     GPIO_Pin_8,
                     GPIO_Mode_OUT,
                     GPIO_Speed_50MHz,
                     GPIO_OType_PP,
                     GPIO_PuPd_NOPULL );

  // Setup tick at 1000Hz
  SysTick_Config( SystemCoreClock/1000 );
  NVIC_SetPriority( SysTick_IRQn, 3 );

  // Create a task for blinking the LED
  xTaskCreate( blinky_task, (const char * const)"LED", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
  xTaskCreate( MS4515_Task, (const char * const)"MS4515", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
  xTaskCreate( MP3V5004_Task, (const char * const)"MP3V5004", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
  xTaskCreate( Print_Task, (const char * const)"Print", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);

  // Start running the scheduler
  vTaskStartScheduler( );

  while ( 1 )
  {
    GPIO_WritePin( &PC8_LED, 0 );
  }

}


// platformio build error unless the _init() is defined
// Adding a blank function to fix linker issues
void _init( void )
{

}
