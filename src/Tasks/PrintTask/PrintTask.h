/****************************************************************************************
 * @File    PrintTask.h
 * @author  SPesut
 * @date    06-March-2016
 *
 * @brief   This file provides the function definitions for a FreeRTOS task that
 *          read the sensor queues and prints using the USART
 *
 ****************************************************************************************/

#ifndef PRINTTASK_H
#define PRINTTASK_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include <stdint.h>
// FreeRTOS headers
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"

/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
 void Print_Task( void* p );


#endif // PRINTTASK_H
