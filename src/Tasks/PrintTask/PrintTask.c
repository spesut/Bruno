/******************************************************************************
 * @File    PrintTask.c
 * @author  SPesut
 * @date    06-March-2016
 *
 * @brief   This file implements a FreeRTOS task that reads the sensor queues
 *          and prints using the USART
 *
 ******************************************************************************/

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include "MS4515Task.h"
#include "MP3V5004Task.h"
#include "PrintTask.h"
#include "USART_Driver.h"
#include "GPIO_Driver.h"


/******************************************************************************
 * Defines
 ******************************************************************************/


/******************************************************************************
 * Module Variables
 ******************************************************************************/
 // USART module object (used for printf redirection)
 static USART_Object_t USART_Object;
 // Pin object for the USART1 pins
 static GPIO_Pin_Object_t USART_Pins;

 /*****************************************************************************
  * Static Function Declarations
  *****************************************************************************/
 static void PrintString( uint8_t * pString );
 static void itoa_uint16( uint16_t value, uint8_t * pString );
 static void ReverseString( uint8_t * pString );

/******************************************************************************
 * Function Definitions
 ******************************************************************************/
/******************************************************************************
 * @brief FreeRTOS task that gets data from two queues and prints it using
 *        the USART.
 *
 * @retval                  None
 ******************************************************************************/
void Print_Task( void* p )
{
  // Configure the USART pins
  GPIO_ConfigurePin( &USART_Pins,
                     GPIO_PORTA,
                     ( GPIO_Pin_9 | GPIO_Pin_10 ),
                     GPIO_Mode_AF,
                     GPIO_Speed_50MHz,
                     GPIO_OType_PP,
                     GPIO_PuPd_NOPULL );
  // Configure the Alternate Functions to mux the USART1 signals
  GPIO_ConfigureAF( &USART_Pins,
                    GPIO_PinSource9,
                    GPIO_AF_1 );
  GPIO_ConfigureAF( &USART_Pins,
                    GPIO_PinSource10,
                    GPIO_AF_1 );
  // Configure the USART1 module (115200 Baud, 8N1)
  USART_ConfigureModule( &USART_Object,
                         USART_MODULE1,
                         115200,
                         USART_WordLength_8b,
                         USART_StopBits_1,
                         USART_Parity_No,
                         ( USART_Mode_Tx | USART_Mode_Rx ),
                         USART_HardwareFlowControl_None );

  // Allocate strings for the two sensors (6 characters covers a uint16)
  uint8_t MS4515String[6];
  uint8_t MP3V5004String[6];

  while ( 1 )
  {
    uint16_t MS4515Reading, MP3V5004Reading;

    // Check to see if both queues have been loaded
    if ( ( uxQueueMessagesWaiting( MS4515_Queue ) != 0 ) &&
         ( uxQueueMessagesWaiting( MP3V5004_Queue ) != 0 ) )
    {
      // Read from both queues
      xQueueReceive( MS4515_Queue, &MS4515Reading, 0 );
      xQueueReceive( MP3V5004_Queue, &MP3V5004Reading, 0 );

      // Convert the sensor readings to strings
      itoa_uint16( MS4515Reading, MS4515String );
      itoa_uint16( MP3V5004Reading, MP3V5004String );

      // Put the data on the USART
      PrintString( MS4515String );
      USART_BlockingWriteByte( &USART_Object, ',' );
      PrintString( MP3V5004String );
      USART_BlockingWriteByte( &USART_Object, '\n' );
      USART_BlockingWriteByte( &USART_Object, '\r' );

      // Send data every ~1s
      vTaskDelay(1000);

    }
  }
}

/******************************************************************************
 * @brief Print a null-terminated string using the USART.
 *
 * @retval                  None
 ******************************************************************************/
static void PrintString( uint8_t * pString )
{
  // Print characters until the null termination is hit
  do {
    USART_BlockingWriteByte( &USART_Object, *pString++ );
  } while ( *pString != '\0' );

}

/******************************************************************************
 * @brief Convert a uint16 to a string.
 *
 * @retval                  None
 ******************************************************************************/
static void itoa_uint16( uint16_t value, uint8_t * pString )
{
  uint8_t * tempString = pString;

  // Convert the value to a base-10 string
  do {
    *tempString++ = value % 10 + '0';
  } while ( ( value /= 10 ) > 0 );

  // Null-terminate the string
  *tempString = '\0';

  // Reverse the order of the string
  ReverseString( pString );

}

/******************************************************************************
 * @brief Reverse the order of a null-terminated string.
 *
 * @retval                  None
 ******************************************************************************/
static void ReverseString( uint8_t * pString )
{

  uint8_t i=0, j, temp;
  uint8_t StringLength;

  // Identify the string length
  while ( pString[i] != '\0' )
  {
    i++;
  }

  // Set the string length
  StringLength = i;

  // Flip the string
  for (i=0, j=StringLength-1; i<j; i++, j-- )
  {
    temp = pString[i];
    pString[i] = pString[j];
    pString[j] = temp;
  }
}
