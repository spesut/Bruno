/****************************************************************************************
 * @File    MP3V5004Task.c
 * @author  SPesut
 * @date    06-March-2016
 *
 * @brief   This file implements a FreeRTOS task for interfacing with the MP3V5004
 *          pressure sensor.
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "MP3V5004Task.h"
#include "ADC_Driver.h"
#include "GPIO_Driver.h"

/****************************************************************************************
 * Defines
 ****************************************************************************************/
// Number of elements to hold in the queue
#define MP3V5004_QUEUE_LENGTH 1
// Number of bits of resolution to be added by oversampling/decimation
#define RESOLUTION_BITS_ADDED 2

/****************************************************************************************
 * Module Variables
 ****************************************************************************************/
 // Queue for the transmitting the MP3V5004 readings
 xQueueHandle MP3V5004_Queue;
 // ADC object for reading from the sensor
 ADC_Object_t ADC_Module;
 // Pin object for the MP3V5004 sensor
 GPIO_Pin_Object_t MP3V5004_Pin;

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief FreeRTOS task that interacts with an MP3V5004 sensor.  The readings are over-
 *        sampled and decimated to provide a 14-bit result.  The pressure reading is
 *        pushed into a queue for another task to read.
 *
 * @retval                  None
 ****************************************************************************************/
void MP3V5004_Task( void* p )
{
  // Configure the ADC module
  ADC_ConfigureModule( &ADC_Module,
                       ADC1,
                       ADC_Resolution_12b,
                       ADC_SampleTime_239_5Cycles );

  // Configure the MP3V5004 pin
  GPIO_ConfigurePin( &MP3V5004_Pin,
                    GPIO_PORTA,
                    GPIO_Pin_1,
                    GPIO_Mode_AN,
                    GPIO_Speed_2MHz,
                    GPIO_OType_PP,
                    GPIO_PuPd_NOPULL );

  // Create the queue for the MP3V5004 readings
  MP3V5004_Queue = xQueueCreate( MP3V5004_QUEUE_LENGTH, sizeof(uint16_t) );

  // Variables to calculate the oversampling/decimation required to increase the
  // resolution of the ADC readings by the desired number of bits
  uint8_t i;
  uint16_t RequiredSamples = 1;
  uint8_t RequiredBitShift = RESOLUTION_BITS_ADDED;

  // Number of samples required is 4^RESOLUTION_BITS_ADDED
  for (i=0; i<RESOLUTION_BITS_ADDED; i++)
  {
    RequiredSamples *= 4;
  }

  while ( 1 )
  {
    // Variable holding the sum of all the ADC samples
    uint16_t ADCSamples=0;

    // Loop until the required number of samples are recorded
    uint16_t SampleCount;
    for ( SampleCount=0; SampleCount<RequiredSamples; SampleCount++ )
    {
      ADCSamples +=
        ADC_BlockingRead( &ADC_Module, ADC_Channel_1, ADC_SampleTime_239_5Cycles );
    }

    // Downsample to get the correct resolution
    ADCSamples >>= RequiredBitShift;

    // Put the data in the queue
    if ( xQueueSend( MP3V5004_Queue, &ADCSamples, 0 )
         == errQUEUE_FULL )
    {
      // Delay a half a second if the queue is full
      vTaskDelay( 500 );
    }
  }
}
