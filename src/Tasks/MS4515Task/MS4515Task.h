/****************************************************************************************
 * @File    MS4515Task.h
 * @author  SPesut
 * @date    06-March-2016
 *
 * @brief   This file provides the function definitions for a FreeRTOS task that
 *          interfaces with the MS4515 pressure sensor.
 *
 ****************************************************************************************/

#ifndef MS4515TASK_H
#define MS4515TASK_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include <stdint.h>
// FreeRTOS headers
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"


/****************************************************************************************
 * Extern variables
 ****************************************************************************************/
extern xQueueHandle MS4515_Queue;


/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
 void MS4515_Task( void* p );


#endif // MS4515TASK_H
