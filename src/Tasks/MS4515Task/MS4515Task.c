/****************************************************************************************
 * @File    MS4515Task.c
 * @author  SPesut
 * @date    06-March-2016
 *
 * @brief   This file implements a FreeRTOS task for interfacing with the MS4515
 *          pressure sensor.
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "MS4515Task.h"
#include "MS4515_Driver.h"

/****************************************************************************************
 * Defines
 ****************************************************************************************/
#define MS4515_QUEUE_LENGTH 1

/****************************************************************************************
 * Module Variables
 ****************************************************************************************/
 // Queue for the transmitting the MS4515 readings
 xQueueHandle MS4515_Queue;

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief FreeRTOS task that interacts with an MS4515 sensor.  The pressure reading is
 *        pushed into a queue for another task to read.
 *
 * @retval                  None
 ****************************************************************************************/
void MS4515_Task( void* p )
{
  // Create the queue for the MS4515 readings
  MS4515_Queue = xQueueCreate( MS4515_QUEUE_LENGTH, sizeof(uint16_t) );

  // Configure the MS4515 device
  MS4515_ConfigureModule();

  // Delay 10ms to let the device warm up
  vTaskDelay( 10 );

  while ( 1 )
  {
    // Structure for reading the MS4515 results
    MS4515_Results_t MS4515Reading;
    // Read the pressure and temperature sensor values
    MS4515_PerformAction( MS4515_READ_DF4, &MS4515Reading );

    // Make sure the reading is valid
    if ( MS4515Reading.status == MS4515_OK )
    {
      // Put the data in the queue
      if ( xQueueSend( MS4515_Queue, &(MS4515Reading.PressureReading), 0 )
           == errQUEUE_FULL )
      {
        // Delay a half a second if the queue is full
        vTaskDelay( 500 );
      }
    }
  }
}
