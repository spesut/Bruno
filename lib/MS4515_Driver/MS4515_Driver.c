/****************************************************************************************
 * @File    MS4515_Driver.c
 * @author  SPesut
 * @date    07-February-2016
 *
 * @brief   This file provides functions to interface with a MS4515 pressure sensor.
 *          Datasheet: http://www.meas-spec.com/downloads/MS4515DO.pdf
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "I2C_Driver.h"
#include "GPIO_Driver.h"
#include "MS4515_Driver.h"


/****************************************************************************************
 * Defines
 ****************************************************************************************/
#define MS4515_ADDRESS  0x8C // May change based on device ordered

/****************************************************************************************
 * Module Variables
 ****************************************************************************************/
// GPIO and I2C objects for interfacing with the MS4515
static I2C_Object_t MS4515_I2C_Port;
static GPIO_Pin_Object_t MS4515_SCL, MS4515_SDA;

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief Configures an I2C Module Object with the desired parameters
 *
 * @param I2C_Object        Pointer to the I2C_Object_t to be configured.
 * @param I2C_Module        Pointer to the I2C module base address (SPL definition)
 *
 * @retval                  None
 ****************************************************************************************/
void MS4515_ConfigureModule( void )
{
  // Configure the Alternate Functions to mux the I2C1 signals
  // MS4515 will use PB6, PB7 for communication
  GPIO_ConfigurePin( &MS4515_SDA,
                     GPIO_PORTB,
                     GPIO_Pin_7,
                     GPIO_Mode_AF,
                     GPIO_Speed_2MHz,
                     GPIO_OType_OD,
                     GPIO_PuPd_NOPULL);
  GPIO_ConfigurePin( &MS4515_SCL,
                    GPIO_PORTB,
                    GPIO_Pin_6,
                    GPIO_Mode_AF,
                    GPIO_Speed_2MHz,
                    GPIO_OType_OD,
                    GPIO_PuPd_NOPULL);
  GPIO_ConfigureAF( &MS4515_SDA,
                    GPIO_PinSource7,
                    GPIO_AF_1 );
  GPIO_ConfigureAF( &MS4515_SCL,
                    GPIO_PinSource6,
                    GPIO_AF_1 );

  // Configure I2C1 to communicate with the MS4515
  I2C_ConfigureModule( &MS4515_I2C_Port, I2C1 );

}


/****************************************************************************************
 * @brief Performs an actions for using the MS4515 pressure sensor
 *
 * @param action            The type of action to be performed
 * @param result            Structure that will be populated with the readings and
 *                          status of the MS4515
 *
 * @retval                  None
 ****************************************************************************************/
void MS4515_PerformAction( MS4515_Actions_t action,
                           MS4515_Results_t * result )
{
  // 4 bytes to read the data from the MS4515
  uint8_t ResultArray[4] = {0};

  switch ( action )
  {
    case MS4515_READ_MR:
      // Send the read address and read 0 bytes to initiate a reading
      I2C_ReadArray( &MS4515_I2C_Port,
                     MS4515_ADDRESS,
                     (uint8_t *)&ResultArray,
                     0 );
      break;
    case MS4515_READ_DF2:
      // Read 2 bytes from the MS4515 ( 14-bit pressure reading )
      I2C_ReadArray( &MS4515_I2C_Port,
                     MS4515_ADDRESS,
                     (uint8_t *)&ResultArray,
                     2 );
      break;
    case MS4515_READ_DF3:
      // Read 3 bytes from the MS4515 ( 14-bit pressure reading and
      // 8-bit temperature reading )
      I2C_ReadArray( &MS4515_I2C_Port,
                     MS4515_ADDRESS,
                     (uint8_t *)&ResultArray,
                     3 );
      break;

    case MS4515_READ_DF4:
      // Read 4 bytes from the MS4515 ( 14-bit pressure reading and
      // 11-bit temperature reading )
      I2C_ReadArray( &MS4515_I2C_Port,
                     MS4515_ADDRESS,
                     (uint8_t *)&ResultArray,
                     4 );
      break;

    // Do nothing if the operation is invalid
    case MS4515_INVALID_ACTION:
      break;


  }

  // The upper 2 bits of the first read byte represent the status of the module
  result->status = ResultArray[0] & 0xC0;
  result->status >>= 6;

  // Populate the result structure
  // First two bytes read from the MS4515 is the pressure reading
  // 14-bit resolution - right-justified data
  result->PressureReading = ( ResultArray[0] << 8 ) | ResultArray[1];
  // Clear the status bits
  result->PressureReading &= ~(0xC0);
  // Second two bytes read from the MS4515 is the temperature reading
  // 11-bit resolution - left-justified data
  // The result struct is populated with right-justified data
  result->TemperatureReading = ( ResultArray[2] << 8 ) | ResultArray[3];
  result->TemperatureReading >>= 5;

}
