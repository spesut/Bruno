/****************************************************************************************
 * @File    MS4515_Driver.h
 * @author  SPesut
 * @date    07-February-2016
 *
 * @brief   This file provides the prototypes for the MS4515 pressure sensor driver.
 *
 ****************************************************************************************/

#ifndef MS4515_DRIVER_H
#define MS4515_DRIVER_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include <stdint.h>


/****************************************************************************************
 * Enum and struct definitions
 ****************************************************************************************/
 /****************************************************************************************
  * @brief Enum defines the different status codes for the MS4515
  ****************************************************************************************/
 typedef enum
 {
   MS4515_OK,
   MS4515_RESERVED,
   MS4515_STALE_DATA,
   MS4515_FAULT_DETECTED,
   MS4515_INVALID_STATUS
 } MS4515_Status_t;


/****************************************************************************************
 * @brief Results structure for data read from an MS4515 pressure sensor.
 *
 * The structure contains the 14-bit pressure reading and 11-bit temperature reading
 * from an MS4515 sensor.  The status of the sensor is provided in this function.
 ****************************************************************************************/
typedef struct
{
	uint16_t PressureReading;
	uint16_t TemperatureReading;
  MS4515_Status_t status;
} MS4515_Results_t;


/****************************************************************************************
 * @brief Enum defines the different actions that can be used to interact with the MS4515
 ****************************************************************************************/
typedef enum
{
  MS4515_READ_MR,         // Initiate measurement
  MS4515_READ_DF2,        // Read 14-bit pressure
  MS4515_READ_DF3,        // Read 14-bit pressure, 8-bit temperature
  MS4515_READ_DF4,        // Read 14-bit pressure, 11-bit temperature
  MS4515_INVALID_ACTION
} MS4515_Actions_t;


/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
  void MS4515_ConfigureModule( void );
  void MS4515_PerformAction( MS4515_Actions_t action,
                             MS4515_Results_t * result );

#endif // MS4515_DRIVER_H
