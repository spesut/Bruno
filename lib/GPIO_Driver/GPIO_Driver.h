/****************************************************************************************
 * @File    GPIO_Driver.h
 * @author  SPesut
 * @date    17-January-2016
 *
 * @brief   This file provides the prototypes for the GPIO Pin object driver
 *
 ****************************************************************************************/

#ifndef GPIO_DRIVER_H
#define GPIO_DRIVER_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_gpio.h"


/****************************************************************************************
 * Typedef/Enums
 ****************************************************************************************/
/****************************************************************************************
 * @brief Definition for a GPIO Pin object.
 *
 * The GPIO Pin object contains two elements from the ST SPL.  These elements are used
 * to maintain the configuration of the GPIO pin.
 ****************************************************************************************/
typedef struct
{
  GPIO_TypeDef * GPIO_Port;
  GPIO_InitTypeDef GPIO_Configuration;
} GPIO_Pin_Object_t;

/****************************************************************************************
 * @brief Enum for the different possible GPIO PORTs
 ****************************************************************************************/
typedef enum
{
  GPIO_PORTA = 0,
  GPIO_PORTB,
  GPIO_PORTC,
  GPIO_PORTD,
  GPIO_PORTE,
  GPIO_PORTF,
  GPIO_INVALID_PORT
} GPIO_PORTS;


/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
void GPIO_ConfigurePin( GPIO_Pin_Object_t * GPIO_PinStruct,
                        GPIO_PORTS GPIO_Port,
                        uint32_t GPIO_Pin,
                        GPIOMode_TypeDef GPIO_Mode,
                        GPIOSpeed_TypeDef GPIO_Speed,
                        GPIOOType_TypeDef GPIO_OType,
                        GPIOPuPd_TypeDef GPIO_PuPd );
void GPIO_TogglePin( GPIO_Pin_Object_t * GPIO_PinStruct );
uint8_t GPIO_ReadPin( GPIO_Pin_Object_t * GPIO_PinStruct );
uint8_t GPIO_ReadOuputPin( GPIO_Pin_Object_t * GPIO_PinStruct );
void GPIO_WritePin( GPIO_Pin_Object_t * GPIO_PinStruct, uint8_t Value );
void GPIO_ConfigureAF( GPIO_Pin_Object_t * GPIO_PinStruct,
                       uint16_t GPIO_PinSource,
                       uint8_t GPIO_AF );

#endif // GPIO_DRIVER_H
