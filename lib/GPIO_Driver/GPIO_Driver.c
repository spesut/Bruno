/****************************************************************************************
 * @File    GPIO_Driver.c
 * @author  SPesut
 * @date    17-January-2016
 *
 * @brief   This file provides functions to configure and utilize a GPIO pin object on the
 *          STM32F0 family of microcontrollers.  The functions enable a GPIO to be read,
 *          written or toggled.
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_rcc.h"
#include "GPIO_Driver.h"

/****************************************************************************************
 * Static Function Declarations
 ****************************************************************************************/
static void GPIO_Enable_AHBClock( GPIO_PORTS GPIO_Port );

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief Configures a GPIO Pin Object with the desired parameters
 *
 * @param GPIO_PinStruct    Pointer to the GPIO_Pin_Object_t to be configured.
 * @param GPIO_Port         Selects which port is used (GPIO_PORTx).
 * @param GPIO_Pin          Selects which pin is used (GPIO_Pin_n).
 * @param GPIO_Mode         Selects the active pin mode.
 * @param GPIO_Speed        Selects the speed of the pin.
 * @param GPIO_OType        Selects the output type of the pin.
 * @param GPIO_PuPd         Configures the pull-up/down of the pin.
 *
 * @retval                  None
 ****************************************************************************************/
void GPIO_ConfigurePin( GPIO_Pin_Object_t * GPIO_PinStruct,
                        GPIO_PORTS GPIO_Port,
                        uint32_t GPIO_Pin,
                        GPIOMode_TypeDef GPIO_Mode,
                        GPIOSpeed_TypeDef GPIO_Speed,
                        GPIOOType_TypeDef GPIO_OType,
                        GPIOPuPd_TypeDef GPIO_PuPd )
{
  // Maintain the port in the object
  switch ( GPIO_Port )
  {
    case GPIO_PORTA:
      GPIO_PinStruct->GPIO_Port = GPIOA;
      break;
    case GPIO_PORTB:
      GPIO_PinStruct->GPIO_Port = GPIOB;
      break;
    case GPIO_PORTC:
      GPIO_PinStruct->GPIO_Port = GPIOC;
      break;
    case GPIO_PORTD:
      GPIO_PinStruct->GPIO_Port = GPIOD;
      break;
    case GPIO_PORTE:
      GPIO_PinStruct->GPIO_Port = GPIOE;
      break;
    case GPIO_PORTF:
      GPIO_PinStruct->GPIO_Port = GPIOF;
      break;
    default:
      break;
  }

  // Fill out the GPIO configuration structure
  GPIO_PinStruct->GPIO_Configuration.GPIO_Pin = GPIO_Pin;
  GPIO_PinStruct->GPIO_Configuration.GPIO_Mode = GPIO_Mode;
  GPIO_PinStruct->GPIO_Configuration.GPIO_Speed = GPIO_Speed;
  GPIO_PinStruct->GPIO_Configuration.GPIO_OType = GPIO_OType;
  GPIO_PinStruct->GPIO_Configuration.GPIO_PuPd = GPIO_PuPd;

  // Enable the clock for the associated port
  GPIO_Enable_AHBClock( GPIO_Port );

  // Initialize the GPIO pin
  GPIO_Init( GPIO_PinStruct->GPIO_Port,
             &( GPIO_PinStruct->GPIO_Configuration ) );

}


/****************************************************************************************
 * @brief Enables the AHB Clock for the desired GPIO PORT.
 *
 * @param GPIO_Port         GPIO_PORTS enum to selects the PORT clock to enable.
 *
 * @retval                  None
 ****************************************************************************************/
static void GPIO_Enable_AHBClock( GPIO_PORTS GPIO_Port )
{
  // Switch case to enable the correct GPIO PORT clock in the AHB
  switch ( GPIO_Port )
  {
    case GPIO_PORTA:
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );
      break;
    case GPIO_PORTB:
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE );
      break;
    case GPIO_PORTC:
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE );
      break;
    case GPIO_PORTD:
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOD, ENABLE );
      break;
    case GPIO_PORTE:
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );
      break;
    case GPIO_PORTF:
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOF, ENABLE );
      break;
    default:
      break;
  }
}


/****************************************************************************************
 * @brief Toggles the selected GPIO pin.
 *
 * @param GPIO_PinStruct    Pointer to the GPIO Pin object to be toggled.
 *
 * @retval                  None
 ****************************************************************************************/
void GPIO_TogglePin( GPIO_Pin_Object_t * GPIO_PinStruct )
{
  uint8_t CurrentPinValue;

  CurrentPinValue = GPIO_ReadOuputPin( GPIO_PinStruct );

  GPIO_WritePin( GPIO_PinStruct,
                 (1^CurrentPinValue) );

}


/****************************************************************************************
 * @brief Function to read the value of a GPIO pin that is configured as an input.
 *
 * This function reads the value of a GPIO pin that is configured as an input.  This is
 * accomplished by wrapping the ST Standard Peripheral Library (SPL)
 * GPIO_ReadInputDataBit() function.
 *
 * @param GPIO_PinStruct    Pointer to the GPIO Pin object that should be read.
 *
 * @retval                  The value on the GPIO input pin.
 ****************************************************************************************/
uint8_t GPIO_ReadPin( GPIO_Pin_Object_t * GPIO_PinStruct )
{
  return GPIO_ReadInputDataBit( GPIO_PinStruct->GPIO_Port,
                                GPIO_PinStruct->GPIO_Configuration.GPIO_Pin );
}


/****************************************************************************************
 * @brief Function to read the value of a GPIO pin that is configured as an output.
 *
 * This function reads the value of a GPIO pin that is configured as an output.  This is
 * accomplished by wrapping the ST Standard Peripheral Library (SPL)
 * GPIO_ReadOutputDataBit() function.
 *
 * @param GPIO_PinStruct    Pointer to the GPIO Pin object that should be read.
 *
 * @retval                  The value currently being driven by the GPIO.
 ****************************************************************************************/
uint8_t GPIO_ReadOuputPin( GPIO_Pin_Object_t * GPIO_PinStruct )
{
  return GPIO_ReadOutputDataBit( GPIO_PinStruct->GPIO_Port,
                                 GPIO_PinStruct->GPIO_Configuration.GPIO_Pin );
}


/****************************************************************************************
 * @brief Function to write a value to a GPIO pin.
 *
 * This function writes the specified value to a GPIO pin.  This is accomplished by
 * wrapping the ST Standard Peripheral Library (SPL) GPIO_WriteBit() function.
 *
 * @param GPIO_PinStruct    Pointer to the GPIO Pin object that will be written to.
 * @param Value             Value to write to the GPIO pin.
 *
 * @revtal                  None
 ****************************************************************************************/
void GPIO_WritePin( GPIO_Pin_Object_t * GPIO_PinStruct, uint8_t Value )
{
  GPIO_WriteBit( GPIO_PinStruct->GPIO_Port,
                 GPIO_PinStruct->GPIO_Configuration.GPIO_Pin,
                 Value );
}


/****************************************************************************************
 * @brief Function to configure the Alternate Function for a GPIO pin.
 *
 * This function sets up the alternate function of the GPIO.  This is accomplished by
 * wrapping the ST Standard Peripheral Library (SPL) GPIO_PinAFConfig() function.
 *
 * @param GPIO_PinStruct    Pointer to the GPIO Pin object that will be written to.
 * @param GPIO_PinSource    Specifies the pin for the Alternate Function.
 * @param GPIO_AF           Selects the alternate function to be used for the pin.
 *
 * @revtal                  None
 ****************************************************************************************/
void GPIO_ConfigureAF( GPIO_Pin_Object_t * GPIO_PinStruct,
                       uint16_t GPIO_PinSource,
                       uint8_t GPIO_AF )
{
  GPIO_PinAFConfig( GPIO_PinStruct->GPIO_Port,
                    GPIO_PinSource,
                    GPIO_AF );
}
