/****************************************************************************************
 * @File    I2C_Driver.c
 * @author  SPesut
 * @date    03-February-2016
 *
 * @brief   This file provides functions to configure and utilize an I2C module object
 *          on the STM32F0 family of microcontrollers.
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_rcc.h"
#include "I2C_Driver.h"


/****************************************************************************************
 * Module defines
 ****************************************************************************************/
#define I2C_TIMEOUT 16384;

/****************************************************************************************
 * Module variables
 ****************************************************************************************/
static uint16_t I2CTimeout = I2C_TIMEOUT;

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief Configures an I2C Module Object with the desired parameters
 *
 * @param I2C_Object        Pointer to the I2C_Object_t to be configured.
 * @param I2C_Module        Pointer to the I2C module base address (SPL definition)
 *
 * @retval                  None
 ****************************************************************************************/
void I2C_ConfigureModule( I2C_Object_t * I2C_Object,
                          I2C_TypeDef * I2C_Module )
{
  // Record the I2C module used in the structure
  I2C_Object->I2C_Module = I2C_Module;

  // Enable the clock to the requested I2C module
  if ( I2C_Module == I2C1 )
  {
    // I2C1 needs the clock utilized to be selected
    RCC_I2CCLKConfig( RCC_I2C1CLK_HSI );
    RCC_APB1PeriphClockCmd( RCC_APB1Periph_I2C1, ENABLE );
  }
  else if ( I2C_Module == I2C2 )
  {
    RCC_APB1PeriphClockCmd( RCC_APB1Periph_I2C2, ENABLE );
  }
  else
  {
    // Need to throw error here
  }

  // Populate the init struct with the default values
  I2C_StructInit( &(I2C_Object->I2C_Configuration) );

  // TODO: Create defines for common I2C frequencies and accept it as
  // an input argument
  I2C_Object->I2C_Configuration.I2C_Timing = 0x00201D2B;
  I2C_Object->I2C_Configuration.I2C_Ack = I2C_Ack_Enable;

  // Initialize the desired module
  I2C_Init( I2C_Object->I2C_Module,
            &(I2C_Object->I2C_Configuration) );

  // Enable the I2C module
  I2C_Cmd( I2C_Object->I2C_Module,
           ENABLE );

}


/****************************************************************************************
 * @brief Reads a configurable number of bytes from an I2C slave register.
 *
 * @param I2C_Object        Pointer to the I2C_Object_t that will be read from.
 * @param SlaveAddress      7-bit slave address that is left-justified.
 * @param RegisterAddress   Register address that reading begins at.
 * @param pData             Pointer to a buffer where the data will be stored.
 * @param NumberOfBytes     Number of bytes to read from the I2C slave.
 *
 * @retval                  None
 ****************************************************************************************/
void I2C_ReadRegArray( I2C_Object_t * I2C_Object,
                       uint8_t SlaveAddress,
                       uint8_t RegisterAddress,
                       uint8_t * pData,
                       uint8_t NumberOfBytes )
{
  // Wait to make sure the I2C isn't busy
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_FLAG_BUSY ) == SET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Send the slave address plus the register address
  I2C_TransferHandling( I2C_Object->I2C_Module,
                        SlaveAddress,
                        1,
                        I2C_SoftEnd_Mode,
                        I2C_Generate_Start_Write );

  // Wait for the slave address to be sent (TXIS flag gets set)
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_ISR_TXIS ) == RESET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Write the register address to be read from
  I2C_SendData( I2C_Object->I2C_Module,
                RegisterAddress );

  // Wait for the data byte transmission to complete (TC flag is set)
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_ISR_TC ) == RESET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Setup the I2C read transaction
  I2C_TransferHandling( I2C_Object->I2C_Module,
                        SlaveAddress,
                        NumberOfBytes,
                        I2C_AutoEnd_Mode,
                        I2C_Generate_Start_Read );

  // Loop until the correct number of bytes is received
  while ( NumberOfBytes-- )
  {
    // Wait for a byte to be received from the slave
    I2CTimeout = I2C_TIMEOUT;
    while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                               I2C_FLAG_RXNE ) == RESET )
    {
      if ( ( I2CTimeout-- ) == 0 )
        return;
    }

    // Put the next byte into the array
    *pData++ = I2C_ReceiveData( I2C_Object->I2C_Module );

  }

}


/****************************************************************************************
 * @brief Reads a configurable number of bytes from an I2C slave that has no registers.
 *
 * @param I2C_Object        Pointer to the I2C_Object_t that will be read from.
 * @param SlaveAddress      7-bit slave address that is left-justified.
 * @param pData             Pointer to a buffer where the data will be stored.
 * @param NumberOfBytes     Number of bytes to read from the I2C slave.
 *
 * @retval                  None
 ****************************************************************************************/
void I2C_ReadArray( I2C_Object_t * I2C_Object,
                    uint8_t SlaveAddress,
                    uint8_t * pData,
                    uint8_t NumberOfBytes )
{
  // Wait to make sure the I2C isn't busy
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_FLAG_BUSY ) == SET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Setup the I2C read transaction
  I2C_TransferHandling( I2C_Object->I2C_Module,
                        SlaveAddress,
                        NumberOfBytes,
                        I2C_AutoEnd_Mode,
                        I2C_Generate_Start_Read );

  // Loop until the correct number of bytes is received
  while ( NumberOfBytes-- )
  {
    // Wait for a byte to be received from the slave
    I2CTimeout = I2C_TIMEOUT;
    while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                               I2C_FLAG_RXNE ) == RESET )
    {
      if ( ( I2CTimeout-- ) == 0 )
        return;
    }

    // Put the next byte into the array
    *pData++ = I2C_ReceiveData( I2C_Object->I2C_Module );

  }

}


/****************************************************************************************
 * @brief Writes a configurable number of bytes to an I2C slave register.
 *
 * @param I2C_Object        Pointer to the I2C_Object_t that will be written to.
 * @param SlaveAddress      7-bit slave address that is left-justified.
 * @param RegisterAddress   Register address that writing begins at.
 * @param pData             Pointer to the data buffer that will be written.
 * @param NumberOfBytes     Number of bytes to send to the I2C slave.
 *
 * @retval                  None
 ****************************************************************************************/
void I2C_WriteRegArray( I2C_Object_t * I2C_Object,
                        uint8_t SlaveAddress,
                        uint8_t RegisterAddress,
                        uint8_t * pData,
                        uint8_t NumberOfBytes )
{
  // Wait to make sure the I2C isn't busy
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_FLAG_BUSY ) == SET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Send the register adress plus the number of bytes
  I2C_TransferHandling( I2C_Object->I2C_Module,
                        SlaveAddress,
                        NumberOfBytes+1,
                        I2C_AutoEnd_Mode,
                        I2C_Generate_Start_Write );

  // Wait for the slave address to be sent (TXIS flag gets set)
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_ISR_TXIS ) == RESET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Write the register address to the slave
  I2C_SendData( I2C_Object->I2C_Module,
                RegisterAddress );

  // Wait for the data byte transmission to complete (TXIS is set)
  I2CTimeout = I2C_TIMEOUT;
  while ( I2C_GetFlagStatus( I2C_Object->I2C_Module,
                             I2C_FLAG_TXIS) == RESET )
  {
    if ( ( I2CTimeout-- ) == 0 )
      return;
  }

  // Loop until the correct number of bytes are transmitted
  while ( NumberOfBytes-- )
  {
    // Write the next data byte to the slave
    I2C_SendData( I2C_Object->I2C_Module,
                  *pData++ );

  }

}


/****************************************************************************************
 * @brief Writes a byte to an I2C slave register.
 *
 * @param I2C_Object        Pointer to the I2C_Object_t that will be written to.
 * @param SlaveAddress      7-bit slave address that is left-justified.
 * @param RegisterAddress   Register address that will be written to.
 * @param Data              Data to write to the register.
 *
 * @retval                  None
 ****************************************************************************************/
void I2C_WriteReg( I2C_Object_t * I2C_Object,
                   uint8_t SlaveAddress,
                   uint8_t RegisterAddress,
                   uint8_t Data )
{
  // Special case of WriteRegArray (only send 1 byte)
  I2C_WriteRegArray( I2C_Object,
                     SlaveAddress,
                     RegisterAddress,
                     &Data,
                     1 );
}
