/****************************************************************************************
 * @File    I2C_Driver.h
 * @author  SPesut
 * @date    03-February-2016
 *
 * @brief   This file provides the prototypes for the I2C Module object driver
 *
 ****************************************************************************************/

#ifndef I2C_DRIVER_H
#define I2C_DRIVER_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_i2c.h"

/****************************************************************************************
 * @brief Definition for an I2C Module object.
 *
 * The I2C module object contains two elements from the ST SPL.  These elements are used
 * to maintain the configuration of the I2C module.
 ****************************************************************************************/
typedef struct
{
	I2C_TypeDef * I2C_Module;
	I2C_InitTypeDef	I2C_Configuration;
} I2C_Object_t;


/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
  void I2C_ConfigureModule( I2C_Object_t * I2C_Object,
                            I2C_TypeDef * I2C_Module );
  void I2C_ReadRegArray( I2C_Object_t * I2C_Object,
                         uint8_t SlaveAddress,
                         uint8_t RegisterAddress,
                         uint8_t * pData,
                         uint8_t NumberOfBytes ) ;
  void I2C_ReadArray( I2C_Object_t * I2C_Object,
                      uint8_t SlaveAddress,
                      uint8_t * pData,
                      uint8_t NumberOfBytes );
  void I2C_WriteRegArray( I2C_Object_t * I2C_Object,
                          uint8_t SlaveAddress,
                          uint8_t RegisterAddress,
                          uint8_t * pData,
                          uint8_t NumberOfBytes );
  void I2C_WriteReg( I2C_Object_t * I2C_Object,
                     uint8_t SlaveAddress,
                     uint8_t RegisterAddress,
                     uint8_t Data );

#endif // I2C_DRIVER_H
