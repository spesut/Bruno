/****************************************************************************************
 * @File    USART_Driver.c
 * @author  SPesut
 * @date    24-January-2016
 *
 * @brief   This file provides functions to configure and utilize a USART object on the
 *          STM32F0 family of microcontrollers.  The functions enable reads/writes to
 *          be performed with the USART module.
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_rcc.h"
#include "USART_Driver.h"

/****************************************************************************************
 * Static Function Declarations
 ****************************************************************************************/
static void USART_Enable_APBClock( USART_MODULES USART_Module );

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief Configures a USART Object with the desired parameters
 *
 * @param USART_Object      Pointer to the USART object that will be initialized.
 * @param USART_Module      Selects the USART module to be used.
 * @param USART_BaudRate    Select the baud rate for the USART.
 * @param USART_WordLength  Selects the data length for the USART.
 * @param USART_StopBits    Selects the number of stop bits to be used.
 * @param USART_Parity      Selects the parity setting for the USART.
 * @param USART_Mode        Selects the mode of operation for the USART.
 * @param USART_HardwareFlowControl
 *                            Selects the flow control settings for the USART.
 *
 * @retval                  None
 ****************************************************************************************/
void USART_ConfigureModule( USART_Object_t * USART_Object,
                            USART_MODULES USART_Module,
                            uint32_t USART_BaudRate,
                            uint32_t USART_WordLength,
                            uint32_t USART_StopBits,
                            uint32_t USART_Parity,
                            uint32_t USART_Mode,
                            uint32_t USART_HardwareFlowControl )
{
  // Maintain the USART of interest in the object
  switch ( USART_Module )
  {
    case USART_MODULE1:
      USART_Object->USART_Module = USART1;
      break;
    case USART_MODULE2:
      USART_Object->USART_Module = USART2;
        break;
    case USART_MODULE3:
      USART_Object->USART_Module = USART3;
      break;
    case USART_MODULE4:
      USART_Object->USART_Module = USART4;
      break;
    case USART_MODULE5:
      USART_Object->USART_Module = USART5;
      break;
    case USART_MODULE6:
      USART_Object->USART_Module = USART6;
      break;
    case USART_MODULE7:
      USART_Object->USART_Module = USART7;
      break;
    case USART_MODULE8:
      USART_Object->USART_Module = USART8;
      break;
    default:
      break;
  }

  // Initialize the USART to the selected settings.
  USART_Object->USART_Configuration.USART_BaudRate = USART_BaudRate;
  USART_Object->USART_Configuration.USART_WordLength = USART_WordLength;
  USART_Object->USART_Configuration.USART_StopBits = USART_StopBits;
  USART_Object->USART_Configuration.USART_Parity = USART_Parity;
  USART_Object->USART_Configuration.USART_Mode = USART_Mode;
  USART_Object->USART_Configuration.USART_HardwareFlowControl = USART_HardwareFlowControl;

  // Enable the clock to the desired USART.
  USART_Enable_APBClock( USART_Module );

  // Initialize the USART module.
  USART_Init( USART_Object->USART_Module,
              &( USART_Object->USART_Configuration ) );

  // Enable the selected USART module.
  USART_Cmd( USART_Object->USART_Module, ENABLE );
}


/****************************************************************************************
 * @brief Enables the APB Clock for the selected USART module.
 *
 * @param USART_Module      USART_MODULES enum enables the appropriate USART module clock.
 *
 * @retval                  None
 ****************************************************************************************/
static void USART_Enable_APBClock( USART_MODULES USART_Module )
{
  // Enable the clock for the selected USART module.
  switch ( USART_Module )
  {
    case USART_MODULE1:
      RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1, ENABLE );
      break;
    case USART_MODULE2:
      RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2, ENABLE );
      break;
    case USART_MODULE3:
      RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3, ENABLE );
      break;
    case USART_MODULE4:
      RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART4, ENABLE );
      break;
    case USART_MODULE5:
      RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART5, ENABLE );
      break;
    case USART_MODULE6:
      RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART6, ENABLE );
      break;
    case USART_MODULE7:
      RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART7, ENABLE );
      break;
    case USART_MODULE8:
      RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART8, ENABLE );
      break;
    default:
      break;
  }
}


/****************************************************************************************
 * @brief Writes out a data byte on the USART module (blocking).
 *
 * @param USART_Object      Pointer to the USART object that will be written to.
 * @param Data              The data to write on the USART module.
 *
 * @retval                  None
 ****************************************************************************************/
void USART_BlockingWriteByte( USART_Object_t * USART_Object, uint8_t Data )
{
  // Wait until the transmit buffer is empty and the send the byte.
  while ( USART_GetFlagStatus( USART_Object->USART_Module,
                               USART_FLAG_TXE ) != SET );
  USART_SendData( USART_Object->USART_Module, Data );
}


/****************************************************************************************
 * @brief Reads a byte from the USART module (blocking).
 *
 * @param USART_Object      Pointer to the USART object that will be read from.
 *
 * @retval                  Returns a 16-bit value read from the USART module.
 ****************************************************************************************/
uint16_t USART_BlockingReadByte( USART_Object_t * USART_Object )
{
  // Wait until data is received and then return the data.
  while ( USART_GetFlagStatus( USART_Object->USART_Module,
                               USART_FLAG_RXNE ) != SET );
  return USART_ReceiveData( USART_Object->USART_Module );
}
