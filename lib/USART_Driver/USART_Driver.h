/****************************************************************************************
 * @File    USART_Driver.h
 * @author  SPesut
 * @date    24-January-2016
 *
 * @brief   This file provides the prototypes for the USART Module object driver
 *
 ****************************************************************************************/

#ifndef USART_DRIVER_H
#define USART_DRIVER_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_usart.h"


/****************************************************************************************
 * Typedef/Enums
 ****************************************************************************************/
/****************************************************************************************
 * @brief Definition for a USART object.
 *
 * The USART object contains two elements from the ST SPL.  These elements are used
 * to maintain the configuration of the GPIO pin.
 ****************************************************************************************/
typedef struct
{
  USART_TypeDef * USART_Module;
  USART_InitTypeDef USART_Configuration;
} USART_Object_t;

/****************************************************************************************
 * @brief Enum for the different possible USART Modules.
 ****************************************************************************************/
typedef enum
{
  USART_MODULE1 = 0,
  USART_MODULE2,
  USART_MODULE3,
  USART_MODULE4,
  USART_MODULE5,
  USART_MODULE6,
  USART_MODULE7,
  USART_MODULE8,
  USART_INVALID_MODULE
} USART_MODULES;


/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
void USART_ConfigureModule( USART_Object_t * USART_Object,
                            USART_MODULES USART_Module,
                            uint32_t USART_BaudRate,
                            uint32_t USART_WordLength,
                            uint32_t USART_StopBits,
                            uint32_t USART_Parity,
                            uint32_t USART_Mode,
                            uint32_t USART_HardwareFlowControl );
void USART_BlockingWriteByte( USART_Object_t * USART_Object, uint8_t Data );
uint16_t USART_BlockingReadByte( USART_Object_t * USART_Object );

#endif // USART_DRIVER_H
