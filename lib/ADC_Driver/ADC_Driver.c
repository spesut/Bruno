/****************************************************************************************
 * @File    ADC_Driver.c
 * @author  SPesut
 * @date    25-January-2016
 *
 * @brief   This file provides functions to configure and utilize an ADC module object
 *          on the STM32F0 family of microcontrollers.  The functions enable the ADC
 *          conversion value to be read.
 *
 ****************************************************************************************/

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_rcc.h"
#include "ADC_Driver.h"

/****************************************************************************************
 * Function Definitions
 ****************************************************************************************/
/****************************************************************************************
 * @brief Configures an ADC Module Object with the desired parameters
 *
 * @param ADC_Object        Pointer to the ADC_Object_t to be configured.
 * @param ADC_Module        Pointer to the ADC module base address (SPL definition)
 * @param ADC_Resolution    Selects the resolution for the ADC.
 * @param ADC_SampleTime    Selects the sample time for the conversion.
 *
 * @retval                  None
 ****************************************************************************************/
void ADC_ConfigureModule( ADC_Object_t * ADC_Object,
                          ADC_TypeDef * ADC_Module,
													uint32_t ADC_Resolution,
  												uint32_t ADC_SampleTime )
{
  // Store the ADC module pointer for future use.
  ADC_Object->ADC_Module = ADC_Module;

  // Enable the clock to the ADC1 module.
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_ADC1, ENABLE );

  // Reset the ADC1 module to its reset values.
	ADC_DeInit( ADC_Object->ADC_Module );

  // Setup the configuration structure with default values.
	ADC_StructInit( &( ADC_Object->ADC_Configuration ) );

  // Setup the configuration structure with the desired settings.
  ADC_Object->ADC_Configuration.ADC_Resolution = ADC_Resolution;
  ADC_Object->ADC_Configuration.ADC_ContinuousConvMode = DISABLE;
  ADC_Object->ADC_Configuration.ADC_ExternalTrigConvEdge =
      ADC_ExternalTrigConvEdge_None;
  ADC_Object->ADC_Configuration.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_Object->ADC_Configuration.ADC_ScanDirection = ADC_ScanDirection_Upward;

  // Initialize the ADC module.
	ADC_Init( ADC_Object->ADC_Module,
            &( ADC_Object->ADC_Configuration ) );

  // Calibrate the ADC module.
	ADC_GetCalibrationFactor( ADC_Object->ADC_Module );

  // Enable the ADC module and wait until the ADC is ready
	ADC_Cmd( ADC_Object->ADC_Module, ENABLE );
	while ( !ADC_GetFlagStatus( ADC_Object->ADC_Module, ADC_FLAG_ADRDY ) );

}


/****************************************************************************************
 * @brief Reads the conversion value from the ADC module (blocking).
 *
 * @param ADC_Object        Pointer to the ADC_Object_t to be configured.
 * @param ADC_Channel       Selects the channel for the ADC conversion.
 * @param ADC_SampleTime    Selects the sample time for the conversion.
 *
 * @retval                  Returns the ADC conversion value for the selected channel.
 ****************************************************************************************/
uint16_t ADC_BlockingRead( ADC_Object_t * ADC_Object,
													 uint32_t ADC_Channel,
                           uint32_t ADC_SampleTime )
{
	// Clear the configured ADC channels
	ADC_Object->ADC_Module->CHSELR = 0;

	// Enable the desired channel
	ADC_ChannelConfig( ADC_Object->ADC_Module,
		 								 ADC_Channel,
										 ADC_SampleTime );

  // Initiate the conversion
  ADC_StartOfConversion( ADC_Object->ADC_Module );
	// Wait for the conversion to complete
	while( ADC_GetFlagStatus( ADC_Object->ADC_Module, ADC_FLAG_EOC) == RESET );

  // Return the ADC conversion value
	return ADC_GetConversionValue( ADC_Object->ADC_Module );
}
