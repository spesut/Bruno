/****************************************************************************************
 * @File    ADC_Driver.h
 * @author  SPesut
 * @date    25-January-2016
 *
 * @brief   This file provides the prototypes for the ADC Module object driver
 *
 ****************************************************************************************/

#ifndef ADC_DRIVER_H
#define ADC_DRIVER_H

/****************************************************************************************
 * Include Files
 ****************************************************************************************/
#include "stm32f0xx_adc.h"

/****************************************************************************************
 * @brief Definition for an ADC Module object.
 *
 * The ADC module object contains two elements from the ST SPL.  These elements are used
 * to maintain the configuration of the ADC module.
 ****************************************************************************************/
typedef struct
{
	ADC_TypeDef * ADC_Module;
	ADC_InitTypeDef	ADC_Configuration;
} ADC_Object_t;


/****************************************************************************************
 * Function declarations
 ****************************************************************************************/
void ADC_ConfigureModule( ADC_Object_t * ADC_Object,
                          ADC_TypeDef * ADC_Module,
													uint32_t ADC_Resolution,
  												uint32_t ADC_SampleTime );
uint16_t ADC_BlockingRead( ADC_Object_t * ADC_Struct,
													 uint32_t ADC_Channel,
                           uint32_t ADC_SampleTime );

#endif // ADC_DRIVER_H
