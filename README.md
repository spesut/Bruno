# Bruno
Project for monitoring the status of an ongoing fermentation.

## Theory of Operation

This project seeks to measure and record the specific gravity of an active
fermentation using a differential pressure sensor.

![BrunoTheory](docs/BrunoTheory.png)

The pressure exerted on the tubing at p1 and p2 is dependent on the depth of the
tubing and the density of the liquid.  The differential sensor provides an output
that is proportional to the difference in pressure between p1 and p2.  By fixing
the length between the two tubes, any change in the pressure reading is solely
based on the changing density of the liquid as fermentation proceeds.  Once the
density of the liquid is known, the specific gravity can be trivially calculated.

## Pressure Sensor Requirements

 - The specific gravity of beer during a fermentation typically falls between 1.000
   and 1.100.  Measurements are typically recorded with three decimal places of
   precision.  A density change of ~0.1% must be accuarately detected by the
   system to provide the necessary precision.
 - For a tubing length difference of 2", there is ~0.074PSI of pressure difference
   between p1 and p2.  As a result, the sensor must operate in a very low pressure
   range.
